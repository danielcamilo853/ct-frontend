export enum IdentiifcationType {
    CedulaCiudadania = 'CEDULA DE CIUDADANIA',
    CedulaExtrangeria = 'CEDULA DE EXTRANJERIA',
    Pasaport = 'PASAPORTE',
    PermisoEspecial = 'PERMISO ESPECIAL'
}