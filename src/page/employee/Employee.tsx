import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Table, Button, Modal } from 'react-bootstrap';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from "yup";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



import { EmployeeFormInf, Employees } from "../../model/employees.interface";
import employeeService from "../../service/employee.service";
import { IdentiifcationType } from "../../enum/IdentificationTypeEnum";
import { Area } from "../../enum/AreaEnum";
import { Country } from "../../enum/CountryEnum";
import './employee.css';


// Table Headers
const columns = ['Id', 'FirstName', 'OtherName', 'firstSurname', 'SecondSurname', 'Email', 'IdType', 'IdNumber', 'Area', 'Country', 'AdmissionDate', 'State', 'Created_at', 'Updated_at'];


// Here i'm transforming enums to array to be able to loop through them and fill the lists 
const idType = Object.keys(IdentiifcationType).map(name => {
  return {
    name,
    value: IdentiifcationType[name as keyof typeof IdentiifcationType]
  }
});

const areas = Object.keys(Area).map(name => {
  return {
    name,
    value: Area[name as keyof typeof Area]
  }
});

const countries = Object.keys(Country).map(name => {
  return {
    name,
    value: Country[name as keyof typeof Country]
  }
});


const Employee: React.FC = () => {

  const [employees, setEmployees] = useState<Employees[]>([]);
  const [show, setShow] = useState<boolean>(false);
  const [createorupdate, setCreateorupdate] = useState<string>('create');
  const [employeeId, setEmployeeId ] = useState<number>(0);

  const [showModalDelete, setShowModalDelete] = useState<boolean>(false);
  const handleCloseModalDelete = () => setShowModalDelete(false);
  const handleShowModalDelete = (id: number) => {
    setShowModalDelete(true)
    setEmployeeId(id);
  };

  //open and close modal
  const handleClose = () => {
    setShow(false)
    setCreateorupdate('create')
    resetValues();
  };
  const handleShow = () => setShow(true);


  // Form validatinos
  const schema = Yup.object().shape({
    firstSurname: Yup.string().required("First surname is required")
      .matches(/^[A-Z]+$/, 'First surname must contain only capital letters')
      .max(20, "First surname can only have 20 characters"),
    secondSurname: Yup.string().required("Second surname is required")
      .matches(/^[A-Z]+$/, 'Second surname must contain only capital letters')
      .max(20, "Second surname can only have 20 characters"),
    firstName: Yup.string().required("First Name is required")
      .matches(/^[A-Z]+$/, 'First Name must contain only capital letters')
      .max(20, "First Name can only have 20 characters"),
    otherName: Yup.string(),
    country: Yup.string().required("Country is required"),
    idType: Yup.string().required("Identification type is required"),
    idNumber: Yup.string().required("Identification number is required")
      .matches(/^[a-z0-9-]+$/i, 'Identification number must contain only alphanumeric'),
    admissionDate: Yup.date().nullable()
    .transform((curr, orig) => orig === '' ? null : curr),
    area: Yup.string().required("Area is required")
  })

  const { register, handleSubmit, reset, setValue, formState: { errors } } = useForm<EmployeeFormInf>({
    resolver: yupResolver(schema)
  });


  const onSubmitCreate = async (data: EmployeeFormInf) => {
    const res = await employeeService.create(data);
    if (res?.status === 201) {
      notify('successful action');
      resetValues();
    }
  };

  const onSubmitUpdate = async(data: EmployeeFormInf) => {
    const res = await employeeService.update(employeeId,  data);
    if (res) {
      notify('successful action');
    }
   
  };

  const notify = (message: string) => {
    toast.success(`${message}`)
    getAll();
  };

  const resetValues = () => {
    reset({ firstSurname: '', secondSurname: '', firstName: '', otherName: '', country: '', idType: '', idNumber: '', area: '', admissionDate: new Date() });
  }

  const fillForm = (employee: Employees) => {
    setCreateorupdate('update');
    setValue('firstSurname', employee.firstSurname)
    setValue('secondSurname', employee.secondSurname)
    setValue('firstName', employee.firstName)
    setValue('otherName', employee.otherName)
    setValue('country', employee.country)
    setValue('idType', employee.idType)
    setValue('idNumber', employee.idNumber)
    setValue('area', employee.area)
    setShow(true);
    setEmployeeId(employee.id);
  }


  const getAll = async () => {
    const res = await employeeService.getAll();
    setEmployees(res?.data);
  }

  const deleteEmployee = async(employeeId: number) => {
    const res = await employeeService.delete(employeeId)
    if (res?.status === 200) {
      handleCloseModalDelete();
      notify('successful action');
      resetValues();
    }
  }



  useEffect(() => {
    getAll();
  }, [])



  return (
    <>
      <header className="header">
        <h1 className="header__title">CIDENET CHALLANGE</h1>
      </header>
      <h2 className="subtitle">Employee Management</h2>
      <ToastContainer />
      <div className="main">
        <div className="create__button">
          <Button variant="secondary" onClick={handleShow}>CREATE</Button>
        </div>
        <div className="table__container">
          <Table striped bordered hover>
            <thead>
              <tr>
                {
                  columns.map(column => {
                    return <th key={column}>{column}</th>
                  })
                }
                <th className="">Update</th>
                <th className="">Delete</th>
              </tr>
            </thead>
            <tbody>
              {
                employees.map(employee => {
                  return (
                    <tr key={employee.id}>
                      <td>{employee.id}</td>
                      <td>{employee.firstName}</td>
                      <td>{employee.otherName}</td>
                      <td>{employee.firstSurname}</td>
                      <td>{employee.secondSurname}</td>
                      <td>{employee.email}</td>
                      <td>{employee.idType}</td>
                      <td>{employee.idNumber}</td>
                      <td>{employee.area}</td>
                      <td>{employee.country}</td>
                      <td>{new Date(employee.admissionDate).toLocaleString()}</td>
                      <td>{employee.state}</td>
                      <td>{new Date(employee.createdAt).toLocaleString()}</td>
                      <td>{new Date(employee.createdAt).toLocaleString()}</td>
                      <td>
                        <button className="btn btn-primary" onClick={() => fillForm(employee)}>UPDATE</button>
                      </td>
                      <td>
                        <button className="btn btn-danger" onClick={()=> handleShowModalDelete(employee.id)}>DELETE</button>
                      </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </Table>
        </div>
      </div>
      <Modal show={show} onHide={handleClose} centered backdrop="static">
        <Modal.Header>
          {
            createorupdate === 'create'
              ?
              <Modal.Title>Register Employee</Modal.Title>

              :
              <Modal.Title>Update Employee</Modal.Title>

          }
        </Modal.Header>
        <form className="p-3" onSubmit={handleSubmit(createorupdate === 'create' ? onSubmitCreate : onSubmitUpdate)}>
          <div className="mb-3 form-group">
            <input type="text" className={`form-control ${errors.firstSurname ? 'is-invalid' : ''}`} placeholder="First Surname"  {...register("firstSurname")} />
            <div className="invalid-feedback">
              {errors.firstSurname?.message}
            </div>
          </div>
          <div className="mb-3">
            <input type="text" className={`form-control ${errors.secondSurname ? 'is-invalid' : ''}`} placeholder="Second Surname" {...register("secondSurname")} />
            <div className="invalid-feedback">
              {errors.secondSurname?.message}
            </div>
          </div>
          <div className="mb-3">
            <input type="text" className={`form-control ${errors.firstName ? 'is-invalid' : ''}`} placeholder="First Name" {...register("firstName")} />
            <div className="invalid-feedback">
              {errors.firstName?.message}
            </div>
          </div>
          <div className="mb-3">
            <input type="text" className={`form-control ${errors.otherName ? 'is-invalid' : ''}`} placeholder="Other Name" {...register("otherName")} />
            <div className="invalid-feedback">
              {errors.otherName?.message}
            </div>
          </div>
          <div className="mb-3">
            <select className={`form-select ${errors.idType ? 'is-invalid' : ''}`} aria-label="Default select example" {...register("idType")}>
              <option defaultValue=""></option>
              {
                idType.map(type => {
                  return <option key={type.name} value={type.value}>{type.value}</option>
                })
              }
            </select>
            <div className="invalid-feedback">
              {errors.idType?.message}
            </div>
          </div>
          <div className="mb-3">
            <input type="text" className={`form-control ${errors.idNumber ? 'is-invalid' : ''}`} placeholder="Identification Number" {...register("idNumber")} />
            <div className="invalid-feedback">
              {errors.idNumber?.message}
            </div>
          </div>
          <div className="mb-3">
            <select className={`form-select ${errors.area ? 'is-invalid' : ''}`} aria-label="Default select example" placeholder="Area" {...register("area")}>
              <option defaultValue=""></option>
              {
                areas.map(area => {
                  return <option key={area.name} value={area.value}>{area.value}</option>
                })
              }
            </select>
            <div className="invalid-feedback">
              {errors.area?.message}
            </div>
          </div>
          <div className="mb-3">
            <select className={`form-select ${errors.country ? 'is-invalid' : ''}`} aria-label="Default select example" {...register("country")}>
              <option defaultValue=""></option>
              {
                countries.map(country => {
                  return <option key={country.name} value={country.value}>{country.value}</option>
                })
              }
            </select>
            <div className="invalid-feedback">
              {errors.country?.message}
            </div>
          </div>
          {
            createorupdate === 'create' ?
            <div className="mb-3">
            <input type="date" className={`form-control ${errors.admissionDate ? 'is-invalid' : ''}`} placeholder="Identification Number"  {...register("admissionDate")} />
            <div className="invalid-feedback">
              {errors.admissionDate?.message}
            </div>
          </div>
          :
          null
          }
        
          {
            createorupdate === 'create'
              ?
              <button type='submit' className="btn btn-primary separeted" >Save</button>
              :
              <button type='submit' className="btn btn-primary separeted" >Update</button>
          }
          <button type="button" className="btn btn-danger" onClick={handleClose}>Close</button>
        </form>
      </Modal>




      <Modal show={showModalDelete} onHide={handleCloseModalDelete} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Employee</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to delete this employee!</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModalDelete}>
            NO
          </Button>
          <Button variant="primary" onClick={() => deleteEmployee(employeeId)}>
            YES
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default Employee