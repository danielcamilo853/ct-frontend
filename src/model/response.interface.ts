import { Employees } from "./employees.interface";

interface ErrorInfo {
    error: string;
    message: string;
    statusCode: number;
}


interface Error {
    response: ErrorInfo;
}

export interface EmployeeResponse {
    status?: number;
    data?: Employees;
    response?: Error;
    code?: string
}