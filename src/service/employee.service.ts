import axios from "axios";
import { EmployeeFormInf } from "../model/employees.interface";
import { EmployeeResponse } from "../model/response.interface";

import { apiUrl } from "../util/apiUrl";

class EmployeeService{

    async getAll() {
        try {
            const response = await axios.get(`${apiUrl}employees`);
            console.log(response.data[0])
            return {
                data: response.data[0],
                count: response.data[1]
            }
        } catch (error) {
            console.log(error)  
        }
    }

    async create(employee: EmployeeFormInf){
        try {
            const response: EmployeeResponse = await axios.post(`${apiUrl}employees`, employee)
            return response;
        } catch (error) {
            console.log(error);
        }
    }

    async update(id: number, employee: EmployeeFormInf ){
        try {
            const response: EmployeeResponse = await axios.put(`${apiUrl}employees/${id}`, employee)
            return(response);
        } catch (error) {
            console.log(error);
        }
    }

    async delete(id: number){
        try {
            const response: EmployeeResponse = await axios.delete(`${apiUrl}employees/${id}`)
            return(response);
        } catch (error) {
            console.log(error);
        }
    }

}

export default new EmployeeService();