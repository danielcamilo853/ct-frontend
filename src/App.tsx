import Employee from './page/employee/Employee';
import './app.css';

function App() {
  return (
    
    <div className="principal">
      <Employee/>
    </div>
    
  );
}

export default App;
