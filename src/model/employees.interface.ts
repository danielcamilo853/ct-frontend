import { Area } from "../enum/AreaEnum";
import { Country } from "../enum/CountryEnum";
import { IdentiifcationType } from "../enum/IdentificationTypeEnum";

export interface Employees {
    id: number;
    firstSurname: string;
    firstName: string;
    secondSurname: string;
    otherName?: string
    country: Country;
    idType: IdentiifcationType;
    idNumber: string;
    admissionDate: Date;
    area: Area;
    email: string;
    state: string;
    createdAt: Date;
    updatedAt: Date;
}

export interface Count {
    count: number;
}

export interface EmployeesAndCount {
    data: Employees[],
    count: Count
}

export interface EmployeeFormInf {
    firstSurname: string;
    firstName: string;
    secondSurname: string;
    otherName?: string
    country: Country | string;
    idType: IdentiifcationType | string;
    idNumber: string;
    admissionDate?: Date;
    area: Area | string;
  };